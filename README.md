# Dockerized Spark Cluster for Kubernetes/OCP

We have used [Red Hat UBI8 OpenJDK](https://catalog.redhat.com/software/containers/ubi8/openjdk-8/5dd6a48dbed8bd164a09589a?container-tabs=overview) as a base image.

In order to build images for multiple architectures, we have used DockerX tool to build for **AMD64**, **ARM64**, and **PPC64LE**

### Build Spark Master Image

```
cd master
docker buildx build --platform linux/amd64,linux/arm64,linux/ppc64le -t dockerhub.tryexa.com/common/spark/spark-master:3.0.2-hadoop3.2 --push .
```
Note: replace **dockerhub.tryexa.com** registry with your own public/private container registry.

### Build Spark Worker Image

```
cd worker
docker buildx build --platform linux/amd64,linux/arm64,linux/ppc64le -t dockerhub.tryexa.com/common/spark/spark-worker:3.0.2-hadoop3.2 --push .
```
Note: replace **dockerhub.tryexa.com** registry with your own public/private container registry.

If you want to change the Spark version of master and worker, please update the Dockerfile in both folders

### How to run spark cluster using docker-compose

```
docker-compose up
```
Note: replace the image names in **docker-compose.yml** with your own image names.

### How to deploy Spark cluster on OCP
In order to build the container images, please follow steps outlined here:

[Spark Master Image](README.md#build-spark-master-image)
&
[Spark Worker Image](README.md#build-spark-worker-image)

To deploy Spark cluster on OCP, 

1) Create a new project 

```
oc new-project testing --display-name="testing"
oc project testing
```

2) Update image tags to reflect Spark Master/Worker in this file
```
deployment/OCP/k8s-spark-cluster.yaml
```

3) Deploy Service and Deployment config files via

```
oc create -f ../deployment/OCP/k8s-spark-cluster.yaml
```

4) To check the status of pods in your desired namespace issue the following

```
[root@cecc-250930-bastion ~]# oc get po -n testing
NAME                            READY   STATUS    RESTARTS   AGE
spark-master-665cd68dc4-fvsxb   1/1     Running   0          3h21m
spark-worker-b8b86b988-5mmsb    1/1     Running   0          3h21m
[root@cecc-250930-bastion ~]# 
```
